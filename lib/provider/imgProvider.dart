import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:aynassign/helpers/db_helper.dart';
import 'package:aynassign/models/ImgModel.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:geocoder/geocoder.dart';
import 'package:location/location.dart';
import 'package:sqflite/sqflite.dart';

class ImgProvider with ChangeNotifier {
  bool loading = false;
  List<ImgModel> imgData = [];
  List<ImgModel> get getImgData {
    return [...imgData];
  }

  void notifyLoading(bool value) {
    loading = value;
    notifyListeners();
  }

  Location location = new Location();
  Future<Map<String, dynamic>> storeImgOffline(File imgPath) async {
    Map<String, dynamic> resp = {'msg': '', 'status': false};
    try {
      notifyLoading(true);
      final featureName = await getLocation();
      imgData.add(ImgModel(img: imgPath, location: featureName));
      notifyLoading(false);
      notifyListeners();
      DBHelper.insert('places', {
        'id': DateTime.now().toIso8601String(),
        'location': featureName,
        'image': imgPath.path
      });
      resp['status'] = true;
      resp['msg'] = 'Image added successfully';
      return resp;
    } catch (e) {
      notifyLoading(false);
      print(e.toString());
      resp['status'] = true;
      resp['msg'] = 'Something went wrong please try again';
      return resp;
    }
  }

  Future<Map<String, dynamic>> storeImgOnline(File img,
      [String featureName]) async {
    Map<String, dynamic> resp = {'msg': '', 'status': false};
    try {
      if (location == null) {
        final address = await getLocation();
        featureName = address;
      }
      notifyLoading(true);
      final res = await http.post('http://127.0.0.1:8000/api/v1/setData',
          body: json.encode({
            'id': DateTime.now().toIso8601String(),
            'img': img.path,
            'location': featureName
          }));
      if (res.statusCode != 200) {
        notifyLoading(false);
        resp['status'] = false;
        resp['msg'] = res.body;
        return resp;
      }
      var response = json.decode(res.body);
      imgData.add(ImgModel(img: img, location: featureName));
      notifyLoading(false);
      notifyListeners();
      resp['status'] = true;
      resp['msg'] = response['msg'];
      return resp;
    } catch (e) {
      notifyLoading(false);

      print(e.toString());
      resp['status'] = true;
      resp['msg'] = 'Something went wrong please try again';
      return resp;
    }
  }

  Future<Map<String, dynamic>> fetchAndSetPlaces() async {
    Map<String, dynamic> resp = {'msg': '', 'status': false};
    try {
      notifyLoading(true);

      final dataList = await DBHelper.getData('places');
      imgData = dataList
          .map((item) => ImgModel(
              id: item['id'],
              img: File(item['image']),
              location: item['location']))
          .toList();
      // print(dataList);
      notifyLoading(false);

      notifyListeners();
    } catch (e) {
      notifyLoading(false);
      print(e.toString());
      resp['status'] = true;
      resp['msg'] = 'Something went wrong please try again';
      return resp;
    }
  }

  Future<Map<String, dynamic>> fetchAndSetOnline() async {
    Map<String, dynamic> resp = {'msg': '', 'status': false};
    try {
      notifyLoading(true);
      final res = await http.get('http://127.0.0.1:8000/api/v1/getData');
      if (res.statusCode != 200) {
        notifyLoading(false);
        resp['status'] = false;
        resp['msg'] = res.body;
        return resp;
      }
      imgData.clear();
      var response = json.decode(res.body) as List<dynamic>;
      response.forEach((el) {
        imgData.add(ImgModel(
            id: el['id'], img: File(el['image']), location: el['location']));
      });
      notifyLoading(false);
      resp['status'] = true;
      resp['msg'] = res.body;
      return resp;
    } catch (e) {
      notifyLoading(false);
      print(e.toString());
      resp['status'] = true;
      resp['msg'] = 'Something went wrong please try again';
      return resp;
    }
  }

  Future<void> checkData() async {
    Map<String, dynamic> resp = {'msg': '', 'status': false};
    try {
      final dataList = await DBHelper.isEmpty();
      print(dataList);
      if (dataList) {
        final dataList = await DBHelper.getData('places');
        final res = await http.post('http://127.0.0.1:8000/api/v1/setListData',
            body: dataList);
      }
    } catch (e) {
      print(e.toString());
    }
  }

  Future<String> getLocation() async {
    final locationData = await location.getLocation();
    final coordinates =
        new Coordinates(locationData.latitude, locationData.longitude);
    final addresses =
        await Geocoder.local.findAddressesFromCoordinates(coordinates);
    return addresses[0].featureName;
  }
}

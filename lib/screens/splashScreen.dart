import 'package:aynassign/functions/nagivation.dart';
import 'package:aynassign/functions/widgetFunction.dart';
import 'package:aynassign/provider/imgProvider.dart';
import 'package:aynassign/screens/homeScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatefulWidget {
  SplashScreen({Key key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  Future<void> checkData() async {
    final imgProvider = Provider.of<ImgProvider>(context, listen: false);
    await imgProvider.checkData();

    //View Purpose
    Future.delayed(Duration(seconds: 5), () {
      Navigator.pushReplacement(context, FadeNavigation(widget: HomeScreen()));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: FutureBuilder(
          future: checkData(),
          builder: (con, snap) => Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                CircleAvatar(
                    radius: 80,
                    backgroundImage: NetworkImage(
                        'https://avatars3.githubusercontent.com/u/49974198?s=460&u=ff4edc506e05d4fd4dc787767a6fccce8c556786&v=4')),
                buildSizedBox(buildHeight(context), 0.05),
                CupertinoActivityIndicator(
                  radius: 25,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

import 'dart:io';

import 'package:aynassign/functions/widgetFunction.dart';
import 'package:aynassign/provider/imgProvider.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:location/location.dart';
import 'package:maps_launcher/maps_launcher.dart';
import 'package:provider/provider.dart';

import 'imageDisplay.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final GlobalKey<ScaffoldState> _scaffoldkey = new GlobalKey<ScaffoldState>();
  bool _serviceEnabled;
  final picker = ImagePicker();

  void getImage() async {
    if (_serviceEnabled) {
      try {
        var result = await Connectivity().checkConnectivity();
        final pickedFile = await picker.getImage(source: ImageSource.camera);
        final imgProvider = Provider.of<ImgProvider>(context, listen: false);
        if (result != ConnectivityResult.none) {
          final res = await imgProvider.storeImgOffline(File(pickedFile.path));
          if (!res['status']) {
            showSnack(context, res['msg'], _scaffoldkey);
          } else {
            showSnack(context, res['msg'], _scaffoldkey);
          }
        } else {
          final res = await imgProvider.storeImgOnline(File(pickedFile.path));
          if (!res['status']) {
            showSnack(context, res['msg'], _scaffoldkey);
          } else {
            showSnack(context, res['msg'], _scaffoldkey);
          }
        }
      } catch (e) {
        print(e.toString());
        showSnack(context, 'Something went wrong!', _scaffoldkey);
      }
      return;
    }
    locationAccess();
  }

  Location location = new Location();
  void locationAccess() async {
    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }
  }

  @override
  void initState() {
    super.initState();
    locationAccess();
  }

  Future<void> getPlaces() async {
    var result = await Connectivity().checkConnectivity();
    final imgProvider = Provider.of<ImgProvider>(context, listen: false);
    if (result != ConnectivityResult.none) {
      final res = await imgProvider.fetchAndSetPlaces();
      if (!res['status']) {
        showSnack(context, res['msg'], _scaffoldkey);
      } else {
        showSnack(context, res['msg'], _scaffoldkey);
      }
    } else {
      final res = await imgProvider.fetchAndSetOnline();
      if (!res['status']) {
        showSnack(context, res['msg'], _scaffoldkey);
      } else {
        showSnack(context, res['msg'], _scaffoldkey);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldkey,
      appBar: AppBar(
        title: Text('AYN'),
        centerTitle: true,
      ),
      floatingActionButton: FloatingActionButton.extended(
          onPressed: () {
            getImage();
          },
          label: Consumer<ImgProvider>(
            builder: (con, load, _) => load.loading
                ? CupertinoActivityIndicator()
                : Row(
                    children: <Widget>[
                      Icon(Icons.camera_enhance),
                      buildSizedBoxWidth(buildWidth(context), 0.02),
                      Text('Click an Image')
                    ],
                  ),
          )),
      body: SafeArea(
          child: FutureBuilder(
        future: getPlaces(),
        builder: (con, snap) => Consumer<ImgProvider>(
          builder: (con, img, _) => img.getImgData.isEmpty
              ? Center(
                  child: Text('You have not uploaded any images yet'),
                )
              : ListView.builder(
                  itemCount: img.getImgData.length,
                  itemBuilder: (con, i) {
                    var items = img.getImgData[i];
                    return Container(
                      height: buildHeight(context) * 0.30,
                      margin:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                          // color: Colors.grey[200],
                          borderRadius: BorderRadius.circular(10),
                          border:
                              Border.all(color: Colors.grey[200], width: 1)),
                      child: Stack(
                        children: [
                          GestureDetector(
                            onTap: () {
                              Navigator.push(context,
                                  MaterialPageRoute(builder: (_) {
                                return ImageDisplay(
                                  imageFile: items.img,
                                );
                              }));
                            },
                            child: Container(
                              margin: EdgeInsets.symmetric(horizontal: 0),
                              width: double.infinity,
                              height: buildHeight(context) * 0.30,
                              child: ClipRRect(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(20),
                                    topRight: Radius.circular(20)),
                                child: Image.file(
                                  items.img,
                                  fit: BoxFit.cover,
                                ),
                              ),
                            ),
                          ),
                          Positioned(
                              top: 10,
                              right: 20,
                              child: IconButton(
                                icon: Icon(Icons.location_on,
                                    size: 38,
                                    color: Theme.of(context).primaryColor),
                                onPressed: () => MapsLauncher.launchQuery(
                                    '${items.location}'),
                              )),
                          Positioned(
                            bottom: 0,
                            right: 0,
                            child: Container(
                              width: 320,
                              padding: EdgeInsets.symmetric(
                                  vertical: 5, horizontal: 20),
                              color: Colors.black54,
                              child: Text(
                                '${items.location}',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    fontSize: 23, color: Colors.white),
                                softWrap: true,
                                overflow: TextOverflow.fade,
                              ),
                            ),
                          )
                        ],
                      ),
                    );
                  }),
        ),
      )),
    );
  }
}

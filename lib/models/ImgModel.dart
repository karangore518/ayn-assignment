import 'dart:io';

class ImgModel {
  String id;
  File img;
  String location;

  ImgModel({this.location, this.img, this.id});
}

import 'package:sqflite/sqflite.dart' as sql;
import 'package:path/path.dart' as path;
import 'package:sqflite/sqlite_api.dart';

class DBHelper {
  static Future<Database> database() async {
    final dbPath = await sql.getDatabasesPath();
    return sql.openDatabase(path.join(dbPath, 'places.db'),
        onCreate: (db, version) {
      return db.execute(
          'CREATE TABLE places(id TEXT PRIMARY KEY,location TEXT,image TEXT)');
    }, version: 1);
  }

  static Future<void> insert(String table, Map<String, Object> data) async {
    final db = await DBHelper.database();
    db.insert(table, data,
        conflictAlgorithm: sql.ConflictAlgorithm
            .replace); //If data is existed the data will be replaced
  }

  static Future<List<Map<String, dynamic>>> getData(String table) async {
    final db = await DBHelper.database();
    return db.query(table);
  }

  static Future<bool> isEmpty() async {
    final db = await DBHelper.database();
    var count = sql.Sqflite.firstIntValue(
        await db.rawQuery('SELECT COUNT(*) FROM places'));
    if (count >= 0) return true;
    return false;
  }
}

import 'package:flutter/material.dart';

SizedBox buildSizedBox(double height, double value) {
  return SizedBox(
    height: height * value,
  );
}

SizedBox buildSizedBoxWidth(double width, double value) {
  return SizedBox(
    width: width * value,
  );
}

double buildHeight(BuildContext context) => MediaQuery.of(context).size.height;

void showSnack(
    BuildContext context, stringList, GlobalKey<ScaffoldState> _scaffoldkey) {
  _scaffoldkey.currentState.showSnackBar(SnackBar(
    behavior: SnackBarBehavior.floating,
    elevation: 8,
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(50)),
    content: Text(
      stringList,
      textAlign: TextAlign.center,
      style: TextStyle(
          fontSize: 13.0, fontWeight: FontWeight.bold, color: Colors.red),
    ),
    duration: Duration(seconds: 2),
    backgroundColor: Colors.black87,
  ));
}

double buildWidth(BuildContext context) => MediaQuery.of(context).size.width;
